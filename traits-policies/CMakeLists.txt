cmake_minimum_required(VERSION 3.5)
project(traits_policies)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

set(SOURCE_FILES main.cpp)
add_executable(traits_policies ${SOURCE_FILES})
