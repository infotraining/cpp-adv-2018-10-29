#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <algorithm>
#include <map>
#include <iterator>

using namespace std;
using namespace Catch::Matchers;

void bar(int)
{
}

namespace TemplateArgTypeDeduction_Case1
{
    template <typename T>
    void foo(T arg)
    {
        puts(__PRETTY_FUNCTION__);
    }
}

TEST_CASE("Template Parameter Deduction - Case 1")
{
    using  TemplateArgTypeDeduction_Case1::foo;

    cout << "Template Parameter Deduction - Case 1\n" << endl;

    foo(1); // int

    int x = 42;
    foo(x); // int
    auto a1 = 42; // int

    const int cx = 13;
    foo(cx); // int
    auto a2 = cx; // int

    int& ref_x = x;
    foo(ref_x); // int
    auto a3 = ref_x; // int


    const int& cref_x = cx;
    foo(cref_x); // int
    auto a4 = cref_x; // int

    int arr[10];
    foo(arr); // int*
    auto a5 = arr; // int*

    foo(bar); // void(*)(int)
    auto a6 = bar; //  void(*)(int)
}

namespace TemplateArgTypeDeduction_Case2
{
    template <typename T>
    void foo(T& arg)
    {
        puts(__PRETTY_FUNCTION__);
    }
}

TEST_CASE("Template Parameter Deduction - Case 2")
{
    using  TemplateArgTypeDeduction_Case2::foo;

    cout << "Template Parameter Deduction - Case 2\n" << endl;

    // foo(1); // ERROR

    int x = 42;
    foo(x); // TArg = int&, T = int
    auto& a1 = x; // int&

    const int cx = 13;
    foo(cx); // TArg = const int&, T = const int
    auto& a2 = cx; // const int&

    int& ref_x = x;
    foo(ref_x); // TArg = int&, T = int
    auto& a3 = ref_x; // int&


    const int& cref_x = cx;
    foo(cref_x); // TArg: const int&, T = const int
    auto& a4 = cref_x; // const int&

    int arr[10];
    foo(arr); // TArg: int(&)[10], T = int[10]
    auto& a5 = arr; // int (&)[10]

    foo(bar); // void(&)(int)
    auto& a6 = bar; // TArg: void(&)(int), T = void(int)
}


namespace TemplateArgTypeDeduction_Case3
{
    template <typename T>
    void foo(T&& arg)
    {
        puts(__PRETTY_FUNCTION__);
    }
}

TEST_CASE("Template Parameter Deduction - Case 3")
{
    using  TemplateArgTypeDeduction_Case3::foo;

    cout << "Template Parameter Deduction - Case 3\n" << endl;

    foo(1); // TArg = int&&, T = int
    auto&& a1 = 1; // int&&

    int x = 42;
    foo(x); // TArg = int&, T = int&
    auto&& a2 = x; // int&

    const int cx = 13;
    foo(cx); // TArg = const int&, T = const int&
    auto&& a3 = cx; // const int&

    int& ref_x = x;
    foo(ref_x); // TArg = int&, T = int&
    auto&& a4 = ref_x; // int&
}

template <typename T>
T max(T a, T b)
{
    return a < b ? b : a;
}

template <typename T>
T* max(T* a, T* b)
{
    return *a < *b ? b : a;
}

namespace Cpp17
{
    template <typename T>
    T max(T a, T b)
    {
        if constexpr(is_pointer_v<T>)
        {
            return *a < *b ? b : a;
        }
        else
        {
            return a < b ? b : a;
        }
    }
}

const char* max(const char* a, const char* b)
{
    return strcmp(a, b) < 0 ? b : a;
}

TEST_CASE("simple templates")
{
    cout << ::max(1, 2) << endl;

    cout << ::max(3.14, 2.71) << endl;

    string str1 = "Ola";
    string str2 = "Ala";

    cout << ::max(str1, str2) << endl;

    SECTION("solving ambigous param deduction")
    {
        // cout << ::max(1, 3.14) << endl; // ERROR - deduction conflict

        cout << ::max<double>(1, 3.14) << endl;

        cout << ::max(static_cast<double>(1), 3.14) << endl;

        cout << ::max("Ola", "Ala") << endl;
    }
}

//template <typename T1, typename T2>
//void foo(T1 a, T2 b)
//{
//    puts(__PRETTY_FUNCTION__);

//    cout << "foo(" << a << ", " << b << ")\n";
//}

//TEST_CASE("many type parameters")
//{
//    foo(1, 3.14); // full deduction

//    foo<int>(5.14, "text"s); // partial deduction

//    foo<string, double>("text", 3); // deduction turned-off
//}

template <typename Iter, typename T>
Iter my_find(Iter start, Iter end, const T& item)
{
    for(auto it = start; it != end; ++it)
    {
        if (*it == item)
            return it;
    }

    return end;
}

template <typename Iter, typename Predicate>
Iter my_find_if(Iter start, Iter end, Predicate pred)
{
    for(auto it = start; it != end; ++it)
    {
        if (pred(*it))
            return it;
    }

    return end;
}

template <typename Iter>
auto my_accumulate(Iter start, Iter end)
{
    using ValueType = decay_t<decltype(*start)>; // since c++11

    //using ValueType = typename iterator_traits<Iter>::value_type;

    ValueType sum = ValueType{};

    for(Iter it = start; it != end; ++it)
        sum += *it;

    return sum;
}

template <typename Iter, typename ValueType>
auto std_accumulate(Iter start, Iter end, ValueType&& init)
{
    ValueType sum = std::forward<ValueType>(init); // copy or move

    for(Iter it = start; it != end; ++it)
        sum += *it;

    return sum;
}

TEST_CASE("std::accumulate with forward")
{
    vector<string> words = { "one", "two", "three" };
    auto result = std_accumulate(words.begin(), words.end(), "Words: "s);

    cout << result << endl;
}


TEST_CASE("Exercise - algorithm")
{
    const vector<int> vec = { 1, 2, 3, 4, 5, 2, 7 };

    SECTION("find")
    {
        SECTION("item is found")
        {
            auto pos = std::find(vec.begin(), vec.end(), 2);

            REQUIRE(pos != vec.end());
            REQUIRE(*pos == 2);
            REQUIRE(distance(vec.begin(), pos) == 1);
        }

        SECTION("item is not found")
        {
            auto pos = my_find(vec.begin(), vec.end(), 9);

            REQUIRE(pos == vec.end());
        }
    }

    SECTION("find_if")
    {
        auto pos = my_find_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0; });

        REQUIRE(*pos == 2);
    }

    SECTION("accmulate")
    {
        auto sum = my_accumulate(vec.begin(), vec.end());

        REQUIRE(sum == 24);
    }
}

TEST_CASE("decltype")
{
    vector<int> vec = { 1, 2, 3, 4 };

    decltype(vec) vec2; // vector<int>

    auto vec3 = vec;

    map<int, string> dict = { { 1, "one"} };

    decltype(dict[0]) item = dict[1];

    REQUIRE(dict.size() == 1);
}


TEST_CASE("for_each in C++11")
{
    vector<int> vec = { 1, 2, 3, 4 };

    for(const auto& item : vec)
    {
        cout << item << endl;
    }

    SECTION("is interpreted as")
    {
        for(auto it = begin(vec); it != end(vec); ++it)
        {
            const auto& item = *it;
            cout << item << " ";
        }
        cout << endl;
    }

    SECTION("works for native array")
    {
        int tab[10] = { 1, 2, 3, 4, 5, 6, 7, 8 };

        for(const auto& item : tab)
        {
            cout << item << " ";
        }
        cout << endl;
     }

}

namespace ExplainStd
{
    template <typename Container>
    auto begin(Container& c)
    {
        return c.begin();
    }

    template <typename Container>
    auto end(Container& c)
    {
        return c.end();
    }

    template <typename Container>
    size_t size(const Container& c)
    {
        return c.size();
    }

    template <typename T, size_t N>
    T* begin(T (&arr)[N])
    {
        return arr;
    }

    template <typename T, size_t N>
    T* end(T (&arr)[N])
    {
        return arr + N;
    }

    template <typename T, size_t N>
    size_t size(T (&arr)[N])
    {
        return N;
    }
}

TEST_CASE("begin, end, size")
{
    int arr[10];

    REQUIRE(ExplainStd::size(arr) == 10);
}
