#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

string full_name(const string &fn, const string &ln)
{
    return fn + " " + ln;
}

TEST_CASE("reference binding")
{
    string first_name = "Jan";

    SECTION("C++98")
    {
        SECTION("l-value reference can bind with l-value")
        {
            string &ref_name = first_name;
        }

        SECTION("r-value can bind with const l-value ref")
        {
            const string &ref_full_name = full_name("Adam", "Nowak");

            cout << ref_full_name << endl;
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can bind with r-value ref")
        {
            string &&rref_full_name = full_name("Adam", "Nowak");

            rref_full_name[0] = 'a';

            cout << rref_full_name << endl;
        }

        SECTION("l-value can not bind with r-value ref")
        {
            // string&& rref_name = first_name; // ERROR
        }
    }
}

void accept_arg(const string &item)
{
    cout << "accept_arg(const string&: " << item << ")\n";
}

void accept_arg(string &&item)
{
    cout << "accept_arg(string&&: " << item << ")\n";
}

TEST_CASE("oveload a function with &&")
{
    string fn = "Jan";

    accept_arg(fn);

    accept_arg(full_name(fn, "Nowak"));
}

////////////////////////////////////////////////////////////////////////////
/// Bitmaps

class Bitmap
{
    char *image_;
    size_t size_;

  public:
    Bitmap(size_t size, char pixel = '*')
        : image_{new char[size + 1]}, size_{size}
    {
        fill_n(image_, size_, pixel);
        image_[size_] = '\0';
        cout << "Bitmap(" << size_ << ", " << pixel << ")\n";
    }

    Bitmap(const Bitmap &source)
        : image_{new char[source.size_ + 1]}, size_{source.size_}
    {
        copy(source.image_, source.image_ + size_, image_);
        image_[size_] = '\0';
        cout << "Bitmap(cc: " << size_ << ", " << image_[0] << ")\n";
    }

    Bitmap &operator=(const Bitmap &source)
    {
        if (this != &source)
        {
            char *temp = new char[source.size_ + 1];
            copy(source.image_, source.image_ + source.size_, temp);
            temp[source.size_] = '\0';

            delete[] image_;
            image_ = temp;
            size_ = source.size_;
        }

        cout << "Bitmap op=(cc: " << size_ << ", " << image_[0] << ")\n";

        return *this;
    }

    // move constructor
    Bitmap(Bitmap &&source) noexcept
        : image_{source.image_}, size_{source.size_}
    {
        source.image_ = nullptr;
        source.size_ = 0; // optional

        cout << "Bitmap(mv: " << size_ << ", " << image_[0] << ")\n";
    }

    // move assignment operator
    Bitmap &operator=(Bitmap &&source) noexcept
    {
        if (this != &source)
        {
            delete[] image_;

            image_ = source.image_;
            source.image_ = nullptr;

            size_ = source.size_;
            source.size_ = 0; // optional
        }

        cout << "Bitmap op=(mv: " << size_ << ", " << image_[0] << ")\n";

        return *this;
    }

    ~Bitmap()
    {
        if (image_ == nullptr)
            cout << "~Bitmap(after move)" << endl;
        else
            cout << "~Bitmap(" << size_ << ", " << image_[0] << ")\n";

        delete[] image_;
    }

    void draw() const
    {
        cout << "Drawing bitmap: " << image_ << "\n";
    }
};

class ISprite
{
  public:
    virtual ~ISprite() = default;
};

class Sprite : public ISprite
{
    string name_ = "default";
    Bitmap bmp_;

  public:
    Sprite(string name, Bitmap bmp)
        : name_{std::move(name)}, bmp_{std::move(bmp)}
    {
    }

    void render() const
    {
        cout << "Rendering sprite: " << name_ << " - ";
        bmp_.draw();
    }
};

TEST_CASE("Sprite")
{
    cout << "\n\n";

    Sprite s1{"car", Bitmap{4, '0'}};
    s1.render();

    vector<Sprite> sprites;
    sprites.push_back(std::move(s1));

    sprites[0].render();
}

namespace LegacyCode
{
    void load_bitmap(Bitmap &bmp, char pixel)
    {
        bmp = Bitmap(24, pixel);
        // ...
    }

    Bitmap *load_bitmap(char pixel)
    {
        Bitmap *bmp = new Bitmap(24, pixel);

        return bmp;
    }
} // namespace LegacyCode

Bitmap load_bitmap(char pixel)
{
    Bitmap bmp(24, pixel);

    return bmp;
}

std::vector<Bitmap> load_bitmaps()
{
    vector<Bitmap> vec_bmps;

    //vec_bmps.reserve(10);

    vec_bmps.push_back(Bitmap(5, ':'));
    vec_bmps.push_back(Bitmap(6, '^'));
    vec_bmps.push_back(Bitmap(7, '%'));
    vec_bmps.push_back(Bitmap(8, 'a'));
    vec_bmps.push_back(Bitmap(9, 'b'));
    vec_bmps.push_back(load_bitmap('+'));

    return vec_bmps;
}

TEST_CASE("Bitmap")
{
    SECTION("creating a bitmap")
    {
        Bitmap bmp1{10, '#'};
        bmp1.draw();

        Bitmap bmp2 = bmp1;
        bmp2.draw();

        Bitmap bmp3{20, '$'};

        bmp1 = bmp3;
        bmp1.draw();

        SECTION("Legacy")
        {
            Bitmap *ptr_bitmap = LegacyCode::load_bitmap('@');
            ptr_bitmap->draw();
            delete ptr_bitmap;
        }

        cout << "\n\n";

        auto bmps = load_bitmaps();

        for (const auto &bmp : bmps)
            bmp.draw();
    }
}

////////////////////////////////////////////////////////
// Rule One of Five

namespace NoDestructor
{
    class X
    {
    public:
        int value = 0;
        string name = "x";

        X(int v, string n)
            : value{v}, name{n}
        {
        }
    };
} // namespace NoDestructor

namespace RuleOneOfFive
{
    class X
    {
    public:
        int value = 0;
        string name = "x";

        X() = default;

        X(int v, string n)
            : value{v}, name{n}
        {
        }

        X(const X &) = default;
        X &operator=(const X &) = default;
        X(X &&) = default;
        X &operator=(X &&) = default;

        ~X()
        {
        }
    };
}
