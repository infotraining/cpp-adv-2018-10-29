#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename T, size_t N>
class Stack
{
public:
    bool empty() const
    {
        return size_ == 0;
    }

    size_t capacity() const
    {
        return N;
    }

    size_t size() const
    {
        return size_;
    }

    void push(const T& item)
    {
        if (stack_is_full())
            throw std::out_of_range("Stack overflow");

        items_[size_] = item;
        ++size_;
    }

    void push(T&& item)
    {
        if (stack_is_full())
            throw std::out_of_range("Stack overflow");

        items_[size_] = std::move(item);
        ++size_;
    }

    T& top()
    {
        assert(size_ >= 1);
        return items_[size_ - 1];
    }

    void pop()
    {
        if (empty())
            throw std::out_of_range("Stack is empty - underflow");

        items_[size_].~T();

        --size_;
    }

private:
    size_t size_ = 0;
    T items_[N];

    bool stack_is_full() const
    {
        return size_ == N;
    }
};


TEST_CASE("Stack - after construction")
{
    Stack<int, 8> s;

    SECTION("is empty")
    {
        REQUIRE(s.empty());
    }

    SECTION("capacity")
    {
        REQUIRE(s.capacity() == 8);
    }

    SECTION("size is zero")
    {
        REQUIRE(s.size() == 0);
    }
}

TEST_CASE("Push item")
{
    Stack<int, 8> s;

    s.push(1);
    s.push(2);

    SECTION("size is increased")
    {
        REQUIRE(s.size() == 2);
    }

    SECTION("last pushed item is on top")
    {
        REQUIRE(s.top() == 2);
    }

    SECTION("when stack overflow std::out_of_range is thrown")
    {
        for(int i = 0; i < 6; ++i)
            s.push(i);

        REQUIRE(s.size() == s.capacity());

        REQUIRE_THROWS_AS(s.push(13), std::out_of_range);
    }
}

TEST_CASE("pop item")
{
    Stack<int, 8> s;

    s.push(1);
    s.push(2);

    s.pop();

    SECTION("size is decreased")
    {
        REQUIRE(s.size() == 1);
    }

    SECTION("pop on empty stack throws out_of_range")
    {
        s.pop();
        REQUIRE(s.size() == 0);
        REQUIRE_THROWS_AS(s.pop(), out_of_range);
    }
}

class Gadget
{
    int id_;
public:
    explicit Gadget(int id) : id_{id}
    {}

    int id() const
    {
        return id_;
    }
};


TEST_CASE("Items without default constructor")
{
    Stack<Gadget, 8> s;

    s.push(Gadget{1});
}
