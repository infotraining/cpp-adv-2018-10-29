#include "stack.hpp"
#include <iostream>

using namespace std;

int main()
{
    Stack<int, 8> s;

    for (int i = 1; i <= 8; ++i)
        s.push(i);

    while (!s.empty())
    {
        int item;
        s.pop(item);

        cout << item << " ";
    }

    cout << endl;
}