#include "catch.hpp"
#include "stack.hpp"
#include <algorithm>
#include <array>

using namespace std;

TEST_CASE("After construction", "[stack]")
{
    Stack<int, 8> s;

    SECTION("is empty")
    {
        REQUIRE(s.empty());
    }

    SECTION("size is zero")
    {
        REQUIRE(s.size() == 0);
    }
}

TEST_CASE("Pushing an item", "[stack,push]")
{
    Stack<int, 8> s;

    SECTION("is no longer empty")
    {
        s.push(1);

        REQUIRE(!s.empty());
    }

    SECTION("size is increased")
    {
        auto size_before = s.size();

        s.push(1);

        REQUIRE(s.size() - size_before == 1);
    }

    SECTION("recently pushed item is on a top")
    {
        s.push(4);

        REQUIRE(s.top() == 4);
    }
}

template <typename T>
vector<T> pop_all(Stack<T>& s)
{
    vector<T> values(s.size());

    for (auto& item : values)
        s.pop(item);

    return values;
}

TEST_CASE("Move semantics", "[stack,push,pop,move]")
{
    SECTION("stores moveable-only objects")
    {
        auto txt1 = make_unique<string>("test1");

        Stack<unique_ptr<string>> s;

        s.push(move(txt1));
        s.push(make_unique<string>("test2"));

        unique_ptr<string> value;

        s.pop(value);
        REQUIRE(*value == "test2"s);

        s.pop(value);
        REQUIRE(*value == "test1"s);
    }

    SECTION("move constructor", "[stack,move]")
    {
        Stack<unique_ptr<string>> s;

        s.push(make_unique<string>("txt1"));
        s.push(make_unique<string>("txt2"));
        s.push(make_unique<string>("txt3"));

        auto moved_s = std::move(s);

        auto values = pop_all(moved_s);

        auto expected = {"txt3", "txt2", "txt1"};
        REQUIRE(std::equal(values.begin(), values.end(), expected.begin(), [](const auto& a, const auto& b) { return *a == b; }));
    }

    SECTION("move assignent", "[stack,move]")
    {
        Stack<unique_ptr<string>> s;

        s.push(make_unique<string>("txt1"));
        s.push(make_unique<string>("txt2"));
        s.push(make_unique<string>("txt3"));

        Stack<unique_ptr<string>> target;
        target.push(make_unique<string>("x"));

        target = std::move(s);

        REQUIRE(target.size() == 3);

        auto values = pop_all(target);

        auto expected = {"txt3", "txt2", "txt1"};
        REQUIRE(std::equal(values.begin(), values.end(), expected.begin(), [](const auto& a, const auto& b) { return *a == b; }));
    }
}

TEST_CASE("Popping an item", "[stack,pop]")
{
    Stack<int, 8> s;

    s.push(1);
    s.push(4);

    int item;

    SECTION("assignes an item from a top to an argument passed by ref")
    {
        s.pop(item);

        REQUIRE(item == 4);
    }

    SECTION("size is decreased")
    {
        size_t size_before = s.size();

        s.pop(item);

        REQUIRE(size_before - s.size() == 1);
    }

    SECTION("LIFO order")
    {
        int a, b;

        s.pop(a);
        s.pop(b);

        REQUIRE(a == 4);
        REQUIRE(b == 1);
    }
}

TEST_CASE("Destruction of objects - LIFO order", "[stack,destructor]")
{
    vector<int> destroyed;
    auto destructor_listener = [&destroyed](int* id) { destroyed.push_back(*id); };

    array<int, 4> ids = {{1, 2, 3, 4}};

    using Recorder = unique_ptr<int, decltype(destructor_listener)>;

    {
        Stack<Recorder> s;

        for (auto& id : ids)
        {
            s.push(Recorder(&id, destructor_listener));
        }
    }

    using Catch::Matchers::Equals;
    REQUIRE_THAT(destroyed, Equals(vector<int>{4, 3, 2, 1}));
}

TEST_CASE("Copy semantics", "[stack,copy]")
{
    Stack<string> s;

    s.push("txt1");
    s.push("txt2");
    s.push("txt3");

    SECTION("copy constructor", "[stack,copy]")
    {
        auto copy_s = s;

        using Catch::Matchers::Equals;
        REQUIRE_THAT(pop_all(copy_s), Equals(std::vector<string>{"txt3", "txt2", "txt1"}));
    }

    SECTION("copy assignment")
    {
        Stack<string> target;
        target.push("x");
        REQUIRE(target.size() == 1);

        target = s;
        REQUIRE(target.size() == 3);

        using Catch::Matchers::Equals;
        REQUIRE_THAT(pop_all(target), Equals(std::vector<string>{"txt3", "txt2", "txt1"}));
    }
}

TEST_CASE("Stack overflow error")
{
    SECTION("when overflow exception is thrown", "[stack,overflow]")
    {
        Stack<int, 8> s;

        for (int i = 0; i < 8; ++i)
            s.push(i);

        REQUIRE_THROWS_AS(s.push(8), std::out_of_range);
    }
}
