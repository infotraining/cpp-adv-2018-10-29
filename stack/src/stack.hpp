#ifndef STACK_HPP
#define STACK_HPP

#include <cassert>
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

template <typename T, size_t Size = 1024>
class Stack
{
public:
    Stack()
        : size_{0}
    {
    }

    Stack(const Stack& source)
        : size_{source.size_}
    {
        for (size_t i = 0; i < size_; ++i)
        {
            new (iterator_at(i)) T(*source.iterator_at(i));
        }
    }

    void clear()
    {
        for (auto it = begin(); it != end(); ++it)
            it->~T();

        size_ = 0;
    }

    Stack& operator=(const Stack& source)
    {
        if (this != &source)
        {
            clear();

            for (size_t i = 0; i < source.size_; ++i)
            {
                new (iterator_at(i)) T(*source.iterator_at(i));
                ++size_;
            }
        }

        return *this;
    }

    Stack(Stack&& source)
        : size_{source.size_}
    {
        for (size_t i = 0; i < size_; ++i)
        {
            new (iterator_at(i)) T(std::move(*source.iterator_at(i)));
        }
    }

    Stack& operator=(Stack&& source)
    {
        if (this != &source)
        {
            clear();

            for (size_t i = 0; i < source.size_; ++i)
            {
                new (iterator_at(i)) T(std::move(*source.iterator_at(i)));
                ++size_;
            }
        }

        return *this;
    }

    ~Stack()
    {
        for (size_t i = 1; i <= size_; ++i)
        {
            T* item = iterator_at(size_ - i);
            item->~T();
        }
    }

    bool empty() const
    {
        return size_ == 0;
    }

    size_t size() const
    {
        return size_;
    }

    void push(const T& item)
    {
        check_overflow();

        new (&data_[size_]) T(item); // placement new
        ++size_;
    }

    void push(T&& item)
    {
        check_overflow();

        new (&data_[size_]) T(std::move(item));
        ++size_;
    }

    const T& top() const
    {
        assert(size_ > 0);
        const T* item_on_top = iterator_at(size_ - 1);
        return *item_on_top;
    }

    void pop(T& item)
    {
        assert(size_ > 0);
        T* item_on_top = iterator_at(size_ - 1);
        item           = std::move(*item_on_top);
        item_on_top->~T();
        --size_;
    }

private:
    using iterator       = T*;
    using const_iterator = const T*;

    std::aligned_storage_t<sizeof(T), alignof(T)> data_[Size];
    size_t size_;

    iterator iterator_at(size_t index)
    {
        return reinterpret_cast<T*>(&data_[index]);
    }

    const_iterator iterator_at(size_t index) const
    {
        return reinterpret_cast<const T*>(&data_[index]);
    }

    iterator begin()
    {
        return iterator_at(0);
    }

    const_iterator begin() const
    {
        return iterator_at(0);
    }

    iterator end()
    {
        return iterator_at(size_);
    }

    const_iterator end() const
    {
        return iterator_at(size_);
    }

    void check_overflow()
    {
        if (size_ == Size)
            throw std::out_of_range("Stack overflow. Maximum capacity is " + std::to_string(Size));
    }
};

#endif
