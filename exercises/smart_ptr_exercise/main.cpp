#include <exception>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vector>

using namespace std;

class Gadget
{
public:
    // konstruktor
    Gadget(int value = 0)
        : value_{value}
    {
        std::cout << "Konstruktor Gadget(" << value_ << ")\n";
    }

    // destruktor
    ~Gadget()
    {
        std::cout << "Destruktor ~Gadget(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};

namespace LegacyCodeLib
{
    Gadget* create_many_gadgets(unsigned int size)
    {
        Gadget* xarray = new Gadget[size];

        for (unsigned int i = 0; i < size; ++i)
            xarray[i].set_value(i);

        return xarray;
    }
}

void reset_value(Gadget& g, int n)
{
    // some logic

    g.set_value(n * n);
    cout << "New value for Gadget: " << g.value() << endl;
}

std::unique_ptr<Gadget> create_gadget(int arg)
{   
    return std::unique_ptr<Gadget>{new Gadget(arg)}; // C++11
}

void unsafe1() // TODO: poprawa z wykorzystaniem smart_ptr
{
    //std::unique_ptr<Gadget> ptr_gdgt = create_gadget(4);
    auto ptr_gdgt = std::make_unique<Gadget>(4);

    /* kod korzystajacy z ptrX */

    reset_value(*ptr_gdgt, 5);

    ptr_gdgt->unsafe();    
}

void unsafe2()
{
    int size = 10;

     std::unique_ptr<Gadget[]> buffer{LegacyCodeLib::create_many_gadgets(size)};

    /* kod korzystający z buffer */

    for (int i = 0; i < size; ++i)
        buffer[0].unsafe();
}

void unsafe3()
{
    vector<std::unique_ptr<Gadget>> my_gadgets;

    my_gadgets.push_back(create_gadget(87));
    my_gadgets.push_back(create_gadget(12));
    my_gadgets.push_back(std::make_unique<Gadget>(23));

    int value_generator = 0;
    for (const auto& item : my_gadgets)
    {
        cout << "Gadget's old value: " << item->value() << endl;
        reset_value(*item, ++value_generator);
    }

    my_gadgets[0].reset();

    my_gadgets[1]->unsafe();
}

int main() try
{
    try
    {
        unsafe1();
        unsafe2();
        unsafe3();
    }
    catch (const exception& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }
}
catch (const exception& e)
{
    cout << "Exception caught: " << e.what() << endl;
}
