#include <iostream>
#include "vector.hpp"

using namespace std;

int main()
{
    Vector<int, ThrowingRangeChecker> vec = {1, 2, 3};

    cout << "vec: ";
    for(auto& item : vec)
        cout << item << " ";
    cout << endl;
}