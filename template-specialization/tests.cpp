#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>
#include <map>

using namespace std;
using namespace Catch::Matchers;


template <typename T>
struct Holder
{
    using value_type = T;

    T obj;

    void info()
    {
        cout << "Holder<T: " << typeid(T).name() << ">(" << obj << ")" << endl;
    }
};

template <typename T>
struct Holder<T*>
{
    using value_type = T;

    T* ptr;

    void info()
    {
        if (ptr)
            cout << "Holder<T*: " << typeid(ptr).name() << ">(" << ptr << ": " << *ptr << ")" << endl;
        else
            cout << "Empty holder." << endl;
    }

    explicit Holder(T* ptr) : ptr{ptr}
    {}

    Holder(const Holder&) = delete;
    Holder& operator=(const Holder&) = delete;

    Holder(Holder&& source) noexcept : ptr{source.ptr}
    {
        source.ptr = nullptr;
    }

    Holder& operator=(Holder&& source) noexcept
    {
        if (this != &source)
        {
            delete ptr;
            ptr = source.ptr;
            source.ptr = nullptr;
        }

        return *this;
    }

    ~Holder()
    {
        cout << "~Holder(" << ptr << ")" << endl;
        delete ptr;
    }

    void reset(T* new_ptr = nullptr)
    {
        delete ptr;
        ptr = new_ptr;
    }
};

template <>
struct Holder<const char*>
{
    using value_type = const char*;

    const char* text;

    void info()
    {
        cout << "Holder<const char*>(" << text << ")" << endl;
    }
};

TEST_CASE("test")
{   
    Holder<string> h1{"text"};
    h1.info();

    Holder<string*> h2{new string("dynamic text")};
    h2.info();
    h2.reset(new string{"new text"});

    auto h3 = std::move(h2);

    const char* text = "Ala";
    Holder<const char*> h4{text};
    h4.info();
}

template <typename T1, typename T2>
struct Pair
{
    T1 first;
    T2 second;

    Pair(T1 f, T2 s) : first{f}, second{s}
    {}
};


TEST_CASE("C++17 - class template argument type deduction")
{
    Pair<int, double> p1{1, 3.14};

    Pair p2{1, 3.14};  // Pair<1, 3.14>

    Pair p3{"text", "text"s}; // Pair<const char*, string>

    int x = 10;
    const int& cref_x = x;

    Pair p4{x, cref_x}; // Pair<int, int>

    vector vec = { 1, 2, 3, 4 };

    std::pair p5{1, "str"s};
}


template <typename ValueT>
using Dictionary = map<string, ValueT>;

TEST_CASE("template alias")
{
    Dictionary<int> dict = { { "one", 1}, {"two", 2} };
}
